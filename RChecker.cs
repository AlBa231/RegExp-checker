using System;
using System.Text;
using System.Text.RegularExpressions;

namespace RegExpChecker
{
	/// <summary>
	/// Summary description for RChecker.
	/// </summary>
	public class RChecker
	{
		private readonly Regex _currentRegEx;

		public RChecker(string pattern, bool multiline)
		{
			var options = multiline ? RegexOptions.Multiline : RegexOptions.Singleline;
			_currentRegEx = new Regex(pattern, options);
		}

		/// <summary>
		/// Parse string and get matches 
		/// </summary>
		/// <param name="text">Text to parse</param>
		/// <param name="multiline"></param>
		/// <returns></returns>
		public string GetMatches(string text, bool multiline)
		{
			var builder = new StringBuilder();
			int i = 0;
			Matches = _currentRegEx.Matches(text);
			foreach(Match match in Matches)
			{
				var printValue = string.IsNullOrEmpty(SelectedGroup) ? match.Value : match.Groups[SelectedGroup].Value;

				if (Numerate)
					builder.AppendFormat("{0} - {1}{2}", i++, printValue, Environment.NewLine);
				else
					builder.AppendFormat("{0}{1}", printValue, Environment.NewLine);
			}
			LastResult = builder.ToString();
			return LastResult;
		}

		/// <summary>
		/// Last Mathes result
		/// </summary>
		public string LastResult { get; private set; }

		/// <summary>
		/// Check this option to numerate resulted matches
		/// </summary>
		public bool Numerate { get; set; }

		public string SelectedGroup { get; set; }

		/// <summary>
		/// Collection of last matches
		/// </summary>
		public MatchCollection Matches { get; private set; }

		public string[] Groups
		{
			get { return _currentRegEx.GetGroupNames(); }
		}

		/// <summary>
		/// Select Next Match Value
		/// </summary>
		/// <param name="tbText"></param>
		public bool SelectNext(System.Windows.Forms.TextBox tbText)
		{
			foreach(Match match in Matches)
			{
				if ((match.Index > tbText.SelectionStart) ||
					(match.Index == tbText.SelectionStart && match.Length != tbText.SelectionLength))
				{
					tbText.Select(match.Index, match.Length);
					tbText.ScrollToCaret();
					return true;
				}
			}
			return false;
		}
	}
}
