using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace RegExpChecker
{
	/// <summary>
	/// Summary description for FindForm.
	/// </summary>
	public class FindForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox tbFind;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Entered Find Text
		/// </summary>
		public string FindText
		{
			get { return tbFind.Text; }
			set { tbFind.Text = value; }
		}

		public FindForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tbFind = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// tbFind
			// 
			this.tbFind.HideSelection = false;
			this.tbFind.Location = new System.Drawing.Point(8, 16);
			this.tbFind.Name = "tbFind";
			this.tbFind.Size = new System.Drawing.Size(208, 20);
			this.tbFind.TabIndex = 0;
			this.tbFind.Text = "";
			this.tbFind.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbFind_KeyPress);
			// 
			// FindForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(226, 48);
			this.ControlBox = false;
			this.Controls.Add(this.tbFind);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "FindForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Find...";
			this.VisibleChanged += new System.EventHandler(this.FindForm_VisibleChanged);
			this.ResumeLayout(false);

		}
		#endregion

		private void tbFind_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (e.KeyChar == ((char)Keys.Escape))
			{
				DialogResult = DialogResult.Cancel;		
				Close();
				return;
			}
			if (e.KeyChar == ((char)Keys.Enter))
			{
				DialogResult = DialogResult.OK;				
				Close();
				return;
			}
			
		}

		private void FindForm_VisibleChanged(object sender, System.EventArgs e)
		{
			tbFind.SelectAll();
		}
	}
}
