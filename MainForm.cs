using System;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace RegExpChecker
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public partial class MainForm : Form
	{
		private RChecker _checker = new RChecker(string.Empty, false);
		private readonly FindForm _findForm;

		public MainForm()
		{
			InitializeComponent();
			_findForm = new FindForm();
		}

		private void btnGetMatch_Click(object sender, EventArgs e)
		{
            var swatch = new System.Diagnostics.Stopwatch();
            swatch.Start();
			_checker = new RChecker(tbRegExp.Text, cbMulti.Checked) {SelectedGroup = cmbGroups.Text, Numerate = cbNumerate.Checked};
            var matches = _checker.GetMatches(tbText.Text, cbMulti.Checked);
            swatch.Stop();
		    lblTimeForExecute.Text = swatch.Elapsed.ToString();
		    tbNewText.Text = matches;
			tbText.SelectionStart = 0;
		}

		private void Form1_Load(object sender, EventArgs e)
		{
		}

		private void btnConvert_Click(object sender, EventArgs e)
		{
			tbRegExp.Text = Regex.Escape(tbRegExp.Text);
		}

		private void btnLoadFrom_Click(object sender, EventArgs e)
		{
			LoadToBox(tbText);
		}

		private void btnFindNext_Click(object sender, EventArgs e)
		{
			if (_checker.Matches == null)
			{
				btnGetMatch_Click(null, null);
			}
			if (!_checker.SelectNext(tbText))
			{
				//MessageBox.Show("Reached the end of text.");
				tbText.SelectionStart = 0;
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			LoadToBox(tbRegExp);
		}

		/// <summary>
		/// Load File to Specified Textbox
		/// </summary>
		/// <param name="tb"></param>
		private void LoadToBox(Control tb)
		{
			if (openFileDialog.ShowDialog() == DialogResult.OK)
			{
				try
				{
					using (var reader = new StreamReader(openFileDialog.FileName))
					{
						tb.Text = reader.ReadToEnd();
					}
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
		}

		private void menuItem2_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void miFind_Click(object sender, EventArgs e)
		{
			if (_findForm.ShowDialog() == DialogResult.OK)
			{
				FindNextText(tbNewText.Focused ? tbNewText : tbText);
			}
		}

		private int TryFind(TextBoxBase tb)
		{
			if (tb.Text.Length == 0) return -1;
			if (tb.SelectionStart == 0 && tb.SelectionLength != _findForm.FindText.Length)
			{
				return tb.Text.IndexOf(_findForm.FindText, 0);
			}
			return tb.Text.IndexOf(_findForm.FindText, tb.SelectionStart + 1);
		}

		private void FindNextText(TextBoxBase tb)
		{
			if (tb.Text.Length == 0) return;
			int index = TryFind(tb);
			if (index == -1)
			{
				tb.SelectionStart = 0;
				tb.SelectionLength = 0;
				index = TryFind(tb);
			}
			if (index <= -1) 
				return;
			tb.Select(index, _findForm.FindText.Length);
			tbText.ScrollToCaret();
		}

		private void menuItem4_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(_findForm.FindText))
				miFind_Click(null, null);
			else
				FindNextText(tbNewText.Focused ? tbNewText : tbText);
		}

		private void menuItem5_Click(object sender, EventArgs e)
		{
			if (tbText.Focused)
				tbText.SelectAll();
			else if (tbNewText.Focused)
				tbNewText.SelectAll();
		}

		private void cbNumerate_CheckedChanged(object sender, EventArgs e)
		{
			_checker.Numerate = cbNumerate.Checked;
		}

		private void tbRegExp_TextChanged(object sender, EventArgs e)
		{
			lblError.Text = string.Empty;
			try
			{
				_checker = new RChecker(tbRegExp.Text, cbMulti.Checked);
			}
			catch (ArgumentException ex)
			{
				lblError.Text = ex.Message;
			}
			ConvertToolStrip.SetToolTip(lblError, lblError.Text); 
			cmbGroups.Items.Clear();
			cmbGroups.Items.AddRange(_checker.Groups);
		}

		private void btnConvertToString_Click(object sender, EventArgs e)
		{
			Clipboard.SetText(tbRegExp.Text.Replace("\"", "\"\""));
		}
	}
}