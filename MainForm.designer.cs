﻿using System.ComponentModel;
using System.Windows.Forms;

namespace RegExpChecker
{
	public partial class MainForm
	{
		private Button btnConvert;
		private Button btnFindNext;
		private Button btnGetMatch;
		private Button btnLoadFrom;
		private Button button1;
		private CheckBox cbMulti;
		private CheckBox cbNumerate;
		private IContainer components;
		private ToolTip ConvertToolStrip;
		private MainMenu mainMenu1;
		private MenuItem menuItem1;
		private MenuItem menuItem2;
		private MenuItem menuItem3;
		private MenuItem menuItem4;
		private MenuItem menuItem5;
		private MenuItem miFind;
		private OpenFileDialog openFileDialog;
		private Panel panel1;
		private Panel panel2;
		private Panel plNewText;
		private Splitter splitter1;
		private TextBox tbNewText;
		private TextBox tbRegExp;
		private ComboBox cmbGroups;
		private Label label1;
		private TextBox tbText;


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			_findForm.Dispose();
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}


		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblError = new System.Windows.Forms.Label();
            this.cmbGroups = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbMulti = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnFindNext = new System.Windows.Forms.Button();
            this.btnLoadFrom = new System.Windows.Forms.Button();
            this.btnConvertToString = new System.Windows.Forms.Button();
            this.btnConvert = new System.Windows.Forms.Button();
            this.cbNumerate = new System.Windows.Forms.CheckBox();
            this.btnGetMatch = new System.Windows.Forms.Button();
            this.tbRegExp = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tbText = new System.Windows.Forms.TextBox();
            this.plNewText = new System.Windows.Forms.Panel();
            this.tbNewText = new System.Windows.Forms.TextBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.ConvertToolStrip = new System.Windows.Forms.ToolTip(this.components);
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.miFind = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.lblTimeForExecuteLabel = new System.Windows.Forms.Label();
            this.lblTimeForExecute = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.plNewText.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblTimeForExecute);
            this.panel1.Controls.Add(this.lblTimeForExecuteLabel);
            this.panel1.Controls.Add(this.lblError);
            this.panel1.Controls.Add(this.cmbGroups);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cbMulti);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.btnFindNext);
            this.panel1.Controls.Add(this.btnLoadFrom);
            this.panel1.Controls.Add(this.btnConvertToString);
            this.panel1.Controls.Add(this.btnConvert);
            this.panel1.Controls.Add(this.cbNumerate);
            this.panel1.Controls.Add(this.btnGetMatch);
            this.panel1.Controls.Add(this.tbRegExp);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(632, 104);
            this.panel1.TabIndex = 5;
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(12, 74);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(0, 13);
            this.lblError.TabIndex = 15;
            // 
            // cmbGroups
            // 
            this.cmbGroups.FormattingEnabled = true;
            this.cmbGroups.Location = new System.Drawing.Point(278, 47);
            this.cmbGroups.Name = "cmbGroups";
            this.cmbGroups.Size = new System.Drawing.Size(110, 21);
            this.cmbGroups.TabIndex = 14;
            this.cmbGroups.SelectedIndexChanged += new System.EventHandler(this.btnGetMatch_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(228, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Groups:";
            // 
            // cbMulti
            // 
            this.cbMulti.Location = new System.Drawing.Point(432, 40);
            this.cbMulti.Name = "cbMulti";
            this.cbMulti.Size = new System.Drawing.Size(120, 24);
            this.cbMulti.TabIndex = 12;
            this.cbMulti.Text = "Multiline";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(320, 16);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(24, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "...";
            this.ConvertToolStrip.SetToolTip(this.button1, "Load Regular Expression From File");
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnFindNext
            // 
            this.btnFindNext.Location = new System.Drawing.Point(8, 45);
            this.btnFindNext.Name = "btnFindNext";
            this.btnFindNext.Size = new System.Drawing.Size(93, 23);
            this.btnFindNext.TabIndex = 10;
            this.btnFindNext.Text = "Find Next";
            this.btnFindNext.Click += new System.EventHandler(this.btnFindNext_Click);
            // 
            // btnLoadFrom
            // 
            this.btnLoadFrom.Location = new System.Drawing.Point(107, 45);
            this.btnLoadFrom.Name = "btnLoadFrom";
            this.btnLoadFrom.Size = new System.Drawing.Size(103, 23);
            this.btnLoadFrom.TabIndex = 9;
            this.btnLoadFrom.Text = "...";
            this.ConvertToolStrip.SetToolTip(this.btnLoadFrom, "Load Text From File");
            this.btnLoadFrom.Click += new System.EventHandler(this.btnLoadFrom_Click);
            // 
            // btnConvertToString
            // 
            this.btnConvertToString.Location = new System.Drawing.Point(107, 74);
            this.btnConvertToString.Name = "btnConvertToString";
            this.btnConvertToString.Size = new System.Drawing.Size(103, 23);
            this.btnConvertToString.TabIndex = 8;
            this.btnConvertToString.Text = "Copy as String";
            this.btnConvertToString.Click += new System.EventHandler(this.btnConvertToString_Click);
            // 
            // btnConvert
            // 
            this.btnConvert.Location = new System.Drawing.Point(8, 74);
            this.btnConvert.Name = "btnConvert";
            this.btnConvert.Size = new System.Drawing.Size(93, 23);
            this.btnConvert.TabIndex = 8;
            this.btnConvert.Text = "Escape Regex";
            this.ConvertToolStrip.SetToolTip(this.btnConvert, "Convert entered text to same equivalent RegEx text.");
            this.btnConvert.Click += new System.EventHandler(this.btnConvert_Click);
            // 
            // cbNumerate
            // 
            this.cbNumerate.Location = new System.Drawing.Point(432, 16);
            this.cbNumerate.Name = "cbNumerate";
            this.cbNumerate.Size = new System.Drawing.Size(120, 24);
            this.cbNumerate.TabIndex = 7;
            this.cbNumerate.Text = "Numerate Matches";
            this.cbNumerate.CheckedChanged += new System.EventHandler(this.cbNumerate_CheckedChanged);
            // 
            // btnGetMatch
            // 
            this.btnGetMatch.Location = new System.Drawing.Point(352, 16);
            this.btnGetMatch.Name = "btnGetMatch";
            this.btnGetMatch.Size = new System.Drawing.Size(72, 23);
            this.btnGetMatch.TabIndex = 6;
            this.btnGetMatch.Text = "Matches";
            this.btnGetMatch.Click += new System.EventHandler(this.btnGetMatch_Click);
            // 
            // tbRegExp
            // 
            this.tbRegExp.Location = new System.Drawing.Point(8, 16);
            this.tbRegExp.Name = "tbRegExp";
            this.tbRegExp.Size = new System.Drawing.Size(304, 20);
            this.tbRegExp.TabIndex = 5;
            this.tbRegExp.TextChanged += new System.EventHandler(this.tbRegExp_TextChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tbText);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 104);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(632, 172);
            this.panel2.TabIndex = 6;
            // 
            // tbText
            // 
            this.tbText.AcceptsReturn = true;
            this.tbText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbText.HideSelection = false;
            this.tbText.Location = new System.Drawing.Point(0, 0);
            this.tbText.MaxLength = 999999999;
            this.tbText.Multiline = true;
            this.tbText.Name = "tbText";
            this.tbText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbText.Size = new System.Drawing.Size(632, 172);
            this.tbText.TabIndex = 3;
            // 
            // plNewText
            // 
            this.plNewText.Controls.Add(this.tbNewText);
            this.plNewText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plNewText.Location = new System.Drawing.Point(0, 276);
            this.plNewText.Name = "plNewText";
            this.plNewText.Size = new System.Drawing.Size(632, 250);
            this.plNewText.TabIndex = 7;
            // 
            // tbNewText
            // 
            this.tbNewText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbNewText.HideSelection = false;
            this.tbNewText.Location = new System.Drawing.Point(0, 0);
            this.tbNewText.MaxLength = 999999999;
            this.tbNewText.Multiline = true;
            this.tbNewText.Name = "tbNewText";
            this.tbNewText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbNewText.Size = new System.Drawing.Size(632, 250);
            this.tbNewText.TabIndex = 4;
            // 
            // splitter1
            // 
            this.splitter1.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 276);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(632, 3);
            this.splitter1.TabIndex = 8;
            this.splitter1.TabStop = false;
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "*.*";
            this.openFileDialog.Filter = "Text Documents|*.txt|HTML files|*.htm;*.html|All files|*.*";
            this.openFileDialog.FilterIndex = 3;
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1,
            this.menuItem3});
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 0;
            this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem2});
            this.menuItem1.Text = "File";
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 0;
            this.menuItem2.Text = "Exit";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 1;
            this.menuItem3.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.miFind,
            this.menuItem4,
            this.menuItem5});
            this.menuItem3.Text = "Edit";
            // 
            // miFind
            // 
            this.miFind.Index = 0;
            this.miFind.Shortcut = System.Windows.Forms.Shortcut.CtrlF;
            this.miFind.Text = "Find...";
            this.miFind.Click += new System.EventHandler(this.miFind_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 1;
            this.menuItem4.Shortcut = System.Windows.Forms.Shortcut.F3;
            this.menuItem4.Text = "Find Next";
            this.menuItem4.Click += new System.EventHandler(this.menuItem4_Click);
            // 
            // menuItem5
            // 
            this.menuItem5.Index = 2;
            this.menuItem5.Shortcut = System.Windows.Forms.Shortcut.CtrlA;
            this.menuItem5.Text = "Select All";
            this.menuItem5.Click += new System.EventHandler(this.menuItem5_Click);
            // 
            // lblTimeForExecuteLabel
            // 
            this.lblTimeForExecuteLabel.AutoSize = true;
            this.lblTimeForExecuteLabel.Location = new System.Drawing.Point(228, 79);
            this.lblTimeForExecuteLabel.Name = "lblTimeForExecuteLabel";
            this.lblTimeForExecuteLabel.Size = new System.Drawing.Size(95, 13);
            this.lblTimeForExecuteLabel.TabIndex = 16;
            this.lblTimeForExecuteLabel.Text = "Time for execute - ";
            // 
            // lblTimeForExecute
            // 
            this.lblTimeForExecute.AutoSize = true;
            this.lblTimeForExecute.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblTimeForExecute.Location = new System.Drawing.Point(329, 79);
            this.lblTimeForExecute.Name = "lblTimeForExecute";
            this.lblTimeForExecute.Size = new System.Drawing.Size(58, 13);
            this.lblTimeForExecute.TabIndex = 16;
            this.lblTimeForExecute.Text = "not runned";
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(632, 526);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.plNewText);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Menu = this.mainMenu1;
            this.Name = "MainForm";
            this.Text = "RegEx Parser";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.plNewText.ResumeLayout(false);
            this.plNewText.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

		private Label lblError;
		private Button btnConvertToString;
        private Label lblTimeForExecute;
        private Label lblTimeForExecuteLabel;
	}
}
